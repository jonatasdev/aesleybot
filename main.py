import telebot
import menus
from menus import InlineKeyboardButton, InlineKeyboardMarkup, Menu
import pagamentos
from pagamentos import Pagamento
from admin import DB
import time
import requests

token = "6279233891:AAGbjtwKtdQ-kebN-hVaVnq2B09fJWcS29o"

bot = telebot.TeleBot(token, parse_mode="markdown")

@bot.callback_query_handler(func=lambda call: True)
def resposta(call):
    print(call)
    match call.data:
        # lista os planos
        case "planos":
            txt = """
*PLANOS DISPONÍVEIS*

=> Emerald
- Conteúdos exclusívos e blablabla
- Suporte

*Para assinar o Emerald:* `/assinar_emerald`

=> Diamond
- Conteúdos exclusivos e blablabla
- sla

*Para assinar o Diamond:* `/assinar_diamond`
"""
            bot.edit_message_text(txt, call.from_user.id, call.message.id)

        case "comprar_1mes":
            mp_page = "https://google.com"
            bot.edit_message_text("Pague seu plano clicando no botão para ir à página de pagamento.", call.from_user.id, call.message.id, reply_markup=Menu.menu_pagamento(mp_page))
        case "verificar":
            resposta = Pagamento.verificar_pagamento('url')
            return resposta
        case "iniciar":
            url = "https://scienceandfilm.org/uploads/videos/files/The_Fountain_%5bTrailer%5d_HD_1080p.mp4"
            bot.edit_message_media(url, call.from_user.id, call.message.id)

@bot.message_handler(commands=["start"])
def iniciar(message):
    bot.reply_to(message, "Bem-vindo. Utilize os botões abaixo para saber mais e adquirir planos.", reply_markup=Menu.menu_principal())

@bot.message_handler(commands=["video"])
def send(message):
    markup = InlineKeyboardMarkup()
    btn = InlineKeyboardButton("iniciar video", callback_data="iniciar")
    markup.add(btn)
    video = "https://scienceandfilm.org/uploads/videos/files/Haxxors-SD.mp4"
    #video = "https://scienceandfilm.org/uploads/videos/files/Operator_Official_Trailer_1_%5bHD%5d_Mae_Whitman_Martin_Starr.mp4"
    bot.send_video(message.from_user.id, video, reply_markup=markup)

@bot.message_handler(commands=["user"])
def search(message):
    msg = message.text
    txt = msg.split()[1]
    usuario = DB.info_assinante(txt)
    try:
        caca = f"""
*ID DO CLIENTE*: `{usuario[0]}`
*PLANO ATUAL*: `{usuario[1]}`
    """
        bot.reply_to(message, caca)
    except:
        bot.reply_to(message, "Usuário não consta no Banco de Dados.")

@bot.message_handler(commands=["criar"])
def criar(message):
    msg = message.text
    user = msg.split()[1]
    plano = msg.split()[2]
    resposta = DB.criar_assinante(user, plano.lower())
    bot.reply_to(message, resposta)

@bot.message_handler(commands=["assinar_emerald"])
def assinatura_emerald(message):
    bot.reply_to(message, "Por quanto tempo deseja assinar o plano EMERALD?", reply_markup=Menu.menu_meses_emerald())

bot.infinity_polling()
