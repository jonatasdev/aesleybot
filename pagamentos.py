import mercadopago

class Pagamento:

    @staticmethod
    def gerar_pagamento_mp(tempo, nivel):
        preco = 0
        if nivel == "EMERALD":
            preco = 15
        elif nivel == "DIAMOND":    
            preco = 17.50
        valor = preco * tempo
        return valor

    def verificar_pagamento(id_pagamento):
        return id_pagamento
