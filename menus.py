import telebot
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton

class Menu:
    def menu_principal():
        markup = InlineKeyboardMarkup()
        planos = InlineKeyboardButton("Planos", callback_data="planos")
        consultar = InlineKeyboardButton("Suporte Admin", url="https://t.me/aeslybr")
        markup.add(planos, consultar)
        return markup

    def menu_pagamento(pagar_url):
        markup = InlineKeyboardMarkup()
        botao_pagar = InlineKeyboardButton("REALIZAR PAGAMENTO", url=pagar_url)
        botao_verifica = InlineKeyboardButton("VERIFICAR PAGAMENTO", callback_data="vericar")
        markup.add(botao_pagar)
        markup.add(botao_verifica)
        return markup

    def menu_meses_emerald():
        markup = InlineKeyboardMarkup()
        btn1 = InlineKeyboardButton("1 MÊS", callback_data="comprar_1mes")
        btn2 = InlineKeyboardButton("3 MESES", callback_data="comprar_3meses")
        btn3 = InlineKeyboardButton("6 MESES", callback_data="comprar_6meses")
        btn4 = InlineKeyboardButton("1 ANO", callback_data="comprar_1ano")
        markup.add(btn1,btn2)
        markup.add(btn3,btn4)
        return markup
